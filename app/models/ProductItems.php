<?php

class ProductItems extends Ukm
{

    /**
     *
     * @var integer
     */
    public $product_item_id;

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $member_store_id;

    /**
     *
     * @var integer
     */
    public $product_category_id;

    /**
     *
     * @var string
     */
    public $product_item_name;

    /**
     *
     * @var string
     */
    public $product_item_year;

    /**
     *
     * @var string
     */
    public $product_item_desc;

    /**
     *
     * @var string
     */
    public $product_item_color;

    /**
     *
     * @var string
     */
    public $product_item_size;

    /**
     *
     * @var integer
     */
    public $product_item_primary;

    /**
     *
     * @var integer
     */
    public $product_item_stock;

    /**
     *
     * @var integer
     */
    public $product_item_current_stock;

    /**
     *
     * @var integer
     */
    public $product_item_price;

    /**
     *
     * @var integer
     */
    public $product_item_price_old;

    /**
     *
     * @var integer
     */
    public $product_item_discount;

    /**
     *
     * @var integer
     */
    public $product_item_active;

    /**
     *
     * @var integer
     */
    public $product_item_use_point;

    /**
     *
     * @var integer
     */
    public $product_item_point;

    /**
     *
     * @var string
     */
    public $product_item_keyword;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('product_item_id', 'OrderDetail', 'product_item_id', array('alias' => 'OrderDetail'));
        $this->hasMany('product_item_id', 'ProductItemMedia', 'product_item_id', array('alias' => 'ProductItemMedia'));
        $this->belongsTo('member_id', 'Members', 'member_id', array('alias' => 'Members'));
        $this->belongsTo('member_store_id', 'MemberStore', 'member_store_id', array('alias' => 'MemberStore'));
        $this->belongsTo('product_category_id', 'ProductCategories', 'product_category_id', array('alias' => 'ProductCategories'));
        $this->belongsTo('product_id', 'Products', 'product_id', array('alias' => 'Products'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_items';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductItems[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductItems
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function add($req){
//        print_r($req);die;
        $productItem = new ProductItems();
        $productItem->product_id = $req->getPost('product_id');
        $productItem->member_id = $req->getPost('member_id');
        $productItem->member_store_id = $req->getPost('member_store_id');
        $productItem->product_category_id = $req->getPost('product_category_id');
        $productItem->product_item_name = $req->getPost('product_item_name');
        $productItem->product_item_year = $req->getPost('product_item_year');
        $productItem->product_item_desc = $req->getPost('product_item_desc');
        $productItem->product_item_color = $req->getPost('product_item_color');
        $productItem->product_item_size = $req->getPost('product_item_size');
        $productItem->product_item_primary = $req->getPost('product_item_primary');
        $productItem->product_item_stock = $req->getPost('product_item_stock');
        $productItem->product_item_current_stock = $req->getPost('product_item_current_stock');
        $productItem->product_item_price = $req->getPost('product_item_price');
        $productItem->product_item_price_old = $req->getPost('product_item_price_old');
        $productItem->product_item_discount = $req->getPost('product_item_discount');
        $productItem->product_item_active = $req->getPost('product_item_active');
        $productItem->product_item_use_point = $req->getPost('product_item_use_point');
        $productItem->product_item_point = $req->getPost('product_item_point');
        $productItem->product_item_keyword = $req->getPost('product_item_keyword');
        if($productItem->save()){
            return $this->returnValue("success");
        }else{
            return $this->returnValue("error");
        }

    }

    public function updateData($id, $req){
        $productItem = ProductItems::findFirst("product_item_id=".$id);
        if(!empty($productItem)){
            $productItem->product_id = $req->getPost('product_id');
            $productItem->member_id = $req->getPost('member_id');
            $productItem->member_store_id = $req->getPost('member_store_id');
            $productItem->product_category_id = $req->getPost('product_category_id');
            $productItem->product_item_name = $req->getPost('product_item_name');
            $productItem->product_item_year = $req->getPost('product_item_year');
            $productItem->product_item_desc = $req->getPost('product_item_desc');
            $productItem->product_item_color = $req->getPost('product_item_color');
            $productItem->product_item_size = $req->getPost('product_item_size');
            $productItem->product_item_primary = $req->getPost('product_item_primary');
            $productItem->product_item_stock = $req->getPost('product_item_stock');
            $productItem->product_item_current_stock = $req->getPost('product_item_current_stock');
            $productItem->product_item_price = $req->getPost('product_item_price');
            $productItem->product_item_price_old = $req->getPost('product_item_price_old');
            $productItem->product_item_discount = $req->getPost('product_item_discount');
            $productItem->product_item_active = $req->getPost('product_item_active');
            $productItem->product_item_use_point = $req->getPost('product_item_use_point');
            $productItem->product_item_point = $req->getPost('product_item_point');
            $productItem->product_item_keyword = $req->getPost('product_item_keyword');
            if($productItem->update()){
                return [
                    'status' => 1,
                    'message' => 'success'
                ];
            }else{
                return [
                    'status' => 0,
                    'message' => 'error'
                ];
            }
        }else{
            return [
                'status' => 0,
                'message' => 'error'
            ];
        }

    }

}
