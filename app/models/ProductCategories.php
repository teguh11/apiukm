<?php

class ProductCategories extends Ukm
{

    /**
     *
     * @var integer
     */
    public $product_category_id;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $member_store_id;

    /**
     *
     * @var string
     */
    public $product_category_title;

    /**
     *
     * @var string
     */
    public $product_category_title_slug;

    /**
     *
     * @var string
     */
    public $product_category_desc;

    /**
     *
     * @var string
     */
    public $product_category_image;

    /**
     *
     * @var integer
     */
    public $product_category_parent;

    /**
     *
     * @var integer
     */
    public $product_category_active;

    /**
     *
     * @var integer
     */
    public $homepage;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('product_category_id', 'OrderDetail', 'product_category_id', array('alias' => 'OrderDetail'));
        $this->hasMany('product_category_id', 'ProductItems', 'product_category_id', array('alias' => 'ProductItems'));
        $this->hasMany('product_category_id', 'Products', 'product_category_id', array('alias' => 'Products'));
        $this->belongsTo('member_store_id', 'MemberStore', 'member_store_id', array('alias' => 'MemberStore'));
        $this->belongsTo('member_id', 'Members', 'member_id', array('alias' => 'Members'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_categories';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductCategories[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductCategories
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    public function beforeValidationOnCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeValidationOnUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function all($request){
        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'pc.member_id IN ('.$p['member_id'].')';
            }
            if(!empty($p['member_store_id'])) {
                $conditions[] = 'pc.member_store_id IN ('.$p['member_store_id'].')';
            }
            if(!empty($p['product_category_active'])) {
                $conditions[] = 'pc.product_category_active = '.$p['product_category_active'].'';
            }
            $conditions = join(' AND ', $conditions);
        }
        $params = [
            'field' => 'pc.*',
            'table' => 'product_categories pc',
//            'order' => 'p.member_id',
//            'sort'  => 'DESC', //optional
            'conditions' => $conditions,
//            'join'  => ['LEFT JOIN member_store ms ON m.member_id = ms.member_id'],
//            'group' => ['pr.product_item_id'],
//            'limit' => [0, 1],
//            'debug' => true
        ];
        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }
    public function add($request){
        $params = $request->getPost();
        $params['product_category_title_slug'] = strtolower(preg_replace('/\s+/', '-', $request->getPost('product_category_title_slug')));
        $params['created_at'] = date('Y-m-d H:i:s');
        $arrResult = ActiveRecord::insert('product_categories',$params);
        return $arrResult;
    }

    public function updateData($request){
        $params = $request->getQuery();
        $id = $params['product_category_id'];
        unset($params['created_at']);
        unset($params['member_id']);
        unset($params['member_store_id']);
        $params['updated_at'] = date('Y-m-d H:i:s');
        $arrResult = ActiveRecord::update('product_categories',$params, 'product_category_id='.$id);
        return $arrResult;
    }
    public function deleteData($request){
        $params = $request->getQuery();
        $id = $params['product_category_id'];
        $arrResult = ActiveRecord::update('product_categories',$params, 'product_category_id='.$id);
        return $arrResult;
    }

}
