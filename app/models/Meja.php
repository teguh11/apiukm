<?php

class Meja extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $meja_id;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $member_store_id;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $label;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'meja';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Meja[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Meja
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    
    public function all($request)
    {
        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'm.member_id IN ('.$p['member_id'].')';
            }
            if(!empty($p['member_store_id'])) {
                $conditions[] = 'm.member_store_id IN ('.$p['member_store_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }
        $params = [
            'field' => 'm.*',
            'table' => 'meja m',
            'conditions' => $conditions,
//            'limit' => [0, 1],
//            'debug' => true
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }
    public static function add($request)
    {
        $params = $request->getPost();
        
        if(isset($params['reset']) && $params['reset']==true) {
            unset($params['label']);
            unset($params['meja_no']);
            unset($params['reset']);
            unset($params['status']);
            $con = 'member_id='.$params['member_id'].' AND member_store_id='.$params['member_store_id'];
            ActiveRecord::remove('meja', $con);
        }
        
        for ($index = 1; $index <= $params['jumlah_meja']; $index++) {
            $data = [
                        'member_id' => $params['member_id'],
                        'member_store_id' => $params['member_store_id'],
                        'meja_no' => $index,
                        'status' => 1,
            ];
            $insert = ActiveRecord::insert('meja',$data);
        }
        if($insert){
            $return = [
                'status' => 1,
                'message' => 'insert berhasil',
            ];
        }else{
            $return = [
                'status' => 0,
                'message' => 'insert gagal',
            ];
        }
        return $return;
    }
    public static function updateData($request)
    {
        $params = $request->getPost();
        
        $conditions = [];
        if(!empty($params)) {
            if(!empty($params['member_id'])) {
                $conditions[] = 'm.member_id IN ('.$params['member_id'].')';
            }
            if(!empty($params['member_store_id'])) {
                $conditions[] = 'm.member_store_id IN ('.$params['member_store_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }
        $arrResult = ActiveRecord::fetchAll([
                'field' => ['m.*'],
                'table' => 'meja m',
                'conditions' => $conditions,
        ]);
        
        foreach ($arrResult["rows"] as $key => $value) {
//            if(isset($params['status'][$value['meja_id']]) || isset($params['label'][$value['meja_id']])){
                $data='';
                if(isset($params['status'][$value['meja_id']])){
                    $data['status'] = $params['status'][$value['meja_id']];
                }
                if(isset($params['meja_no'][$value['meja_id']])){
                    $data['meja_no'] = $params['meja_no'][$value['meja_id']];
                }
                if(isset($params['label'][$value['meja_id']])) {
                    $data['label'] = $params['label'][$value['meja_id']];
                }
//                echo '<pre>';
//                var_dump($data); die;
                $insert = ActiveRecord::update('meja',$data,'meja_id='.$value['meja_id']);
//            }
        }
        if($insert){
            $return = [
                'status' => 1,
                'message' => 'insert berhasil',
            ];
        }else{
            $return = [
                'status' => 0,
                'message' => 'insert gagal',
            ];
        }
        return $return;
    }

}
