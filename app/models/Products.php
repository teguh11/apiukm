<?php
//use ActiveRecord;

class Products extends Ukm
{

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $member_store_id;

    /**
     *
     * @var integer
     */
    public $product_category_id;

    /**
     *
     * @var string
     */
    public $product_title;

    /**
     *
     * @var string
     */
    public $product_title_slug;

    /**
     *
     * @var integer
     */
    public $product_active;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('product_id', 'OrderDetail', 'product_id', array('alias' => 'OrderDetail'));
        $this->hasMany('product_id', 'ProductItems', 'product_id', array('alias' => 'ProductItems'));
        $this->belongsTo('member_id', 'Members', 'member_id', array('alias' => 'Members'));
        $this->belongsTo('member_store_id', 'MemberStore', 'member_store_id', array('alias' => 'MemberStore'));
        $this->belongsTo('product_category_id', 'ProductCategories', 'product_category_id', array('alias' => 'ProductCategories'));
    }

    public function validation()
    {
        $this->validate(new Uniqueness(array(
            "field"   => "product_title",
            "condition" => [
                "member_id = 1",
                "member_store_id = 1",
            ],
            "message" => "Value of field 'product title' is already present in another record"
        )));
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'products';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Products[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Products
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function all($request){

        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['product_id'])) {
                $conditions[] = 'p.product_id IN ('.$p['product_id'].')';
            }
            if(!empty($p['product_active'])) {
                $conditions[] = 'p.product_active = '.$p['product_active'].'';
            }
            if(!empty($p['product_title'])) {
                $conditions[] = 'p.product_title like '.$p['product_title'].'';
            }
            if(!empty($p['member_id'])){
                $conditions[] = 'p.member_id IN ('.$p['member_id'].')';
            }
            if(!empty($p['member_store_id'])){
                $conditions[] = 'p.member_store_id IN ('.$p['member_store_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }

        $params = [
            'field' => 'p.*,pr.*,pim.media_title, pc.*',
            'table' => 'products p',
            'order' => 'p.product_id',
            'sort'  => 'DESC', //optional
            'conditions' => $conditions,
            'join'  => [
                'JOIN product_items pr ON p.product_id = pr.product_id',
                'JOIN product_item_media pim ON pim.product_item_id = pr.product_item_id',
                'JOIN product_categories pc ON pc.product_category_id = pr.product_category_id'
            ],
            'group' => ['pr.product_item_id'],
//            'limit' => [0, 1],
//            'debug' => true
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function add($request){
        $data = $request->getPost();
        $params['member_id'] = $request->getPost('member_id');
        $params['member_store_id'] = $request->getPost('member_store_id');
        $params['product_category_id'] = $request->getPost('product_category_id');
        $params['product_title'] = $request->getPost('product_title');
        $params['product_title_slug'] = strtolower(preg_replace('/\s+/', '-', $data['product_title']));
        $params['created_at'] = date('Y-m-d H:i:s');
        $params['product_active'] = $request->getPost('active');

        $arrResult = ActiveRecord::insert('products',$params);

        $productId = $arrResult['lastInsertId'];
        $result = $this->addProductItem($productId, $request);

        return $result;
    }

    function addProductItem($productId = 0, $data){
        $params['product_id'] = $productId;
        $params['member_id'] = $data->getPost('member_id');
        $params['member_store_id'] = $data->getPost('member_store_id');
        $params['product_category_id'] = $data->getPost('product_category_id');
        $params['product_item_name'] = $data->getPost('product_title');
        $params['product_item_desc'] = $data->getPost('item_desc');
        $params['product_item_stock'] = $data->getPost('item_stock');
        $params['product_item_current_stock'] = $data->getPost('item_stock');
        $params['product_item_price'] = $data->getPost('item_price');
        $params['product_item_year'] = date('Y');
        $params['created_at'] = date('Y-m-d H:i:s');
        $params['updated_at'] = date('Y-m-d H:i:s');
        $productItem = ActiveRecord::insert('product_items', $params);

        return $productItem;
    }

    public function updateData($request){

        $params = $request->getQuery();
        $where = 'product_id='.$params['product_id'];
        unset($params['product_id']);
        unset($params['_url']);
        unset($params['created_at']);
        $params['updated_at'] = date('Y-m-d H:i:s');
        $arrResult = ActiveRecord::update('products',$params, $where);
        return $arrResult;
    }

    public function allBarang($request){

        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['product_id'])) {
                $conditions[] = 'p.product_id IN ('.$p['product_id'].')';
            }
            if(!empty($p['product_active'])) {
                $conditions[] = 'p.product_active = '.$p['product_active'].'';
            }
            if(!empty($p['product_title'])) {
                $conditions[] = 'p.product_title like '.$p['product_title'].'';
            }
            $conditions = join(' AND ', $conditions);
        }

        $params = [
            'field' => 'c.*,p.*',
            'table' => 'products p',
            'order' => 'p.product_id',
            'sort'  => 'DESC', //optional
            'conditions' => $conditions,
            'join'  => ['INNER JOIN product_items pr ON p.product_id = pr.product_id'],
//            'group' => ['pr.product_item_id'],
//            'limit' => [0, 1],
//            'debug' => true
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function detail($req){
        $params = [
            'field' => '*',
            'table' => 'products p',
            'join'  => [
                'LEFT JOIN product_categories pc on pc.product_category_id = p.product_category_id',
                'LEFT JOIN product_items pi on p.product_id = pi.product_id',
                'LEFT JOIN product_item_media pim on pi.product_item_id = pim.product_item_id',
            ],
            'conditions' => [
                'pi.product_item_id = '.$req->getQuery("id")
            ],
            'limit' => [0,1],
            'debug' => false
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        $result = [];
        for($i=0; $i<count($arrResult['rows']); $i++){
            $arrResult['rows'][$i]['media_url'] = $this->getDI()->getConfig()->application->imageUrl.$arrResult['rows'][$i]['media_path'];
        }
        if($arrResult['total_rows'] > 0){
            $result = [
                'status' => 1,
            ];
            $result = array_merge($result, $arrResult);
        }else{
            $result = [
                'status' => 1,
                'message' => 'Data yang anda cari tidak ditemukan'
            ];
        }
        return $result;
    }

}
