<?php
use ActiveRecord;
class OrderDetail extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $order_detail_id;

    /**
     *
     * @var integer
     */
    public $order_id;

    /**
     *
     * @var integer
     */
    public $order_price;

    /**
     *
     * @var integer
     */
    public $order_use_point;

    /**
     *
     * @var integer
     */
    public $order_point;

    /**
     *
     * @var integer
     */
    public $order_qty;

    /**
     *
     * @var string
     */
    public $order_resi;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $seller_id;

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     *
     * @var integer
     */
    public $product_item_id;

    /**
     *
     * @var integer
     */
    public $product_category_id;

    /**
     *
     * @var string
     */
    public $product_category;

    /**
     *
     * @var string
     */
    public $product_category_parent;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $update_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('member_id', 'Members', 'member_id', array('alias' => 'Members'));
        $this->belongsTo('order_id', 'Orders', 'order_id', array('alias' => 'Orders'));
        $this->belongsTo('product_category_id', 'ProductCategories', 'product_category_id', array('alias' => 'ProductCategories'));
        $this->belongsTo('product_item_id', 'ProductItems', 'product_item_id', array('alias' => 'ProductItems'));
        $this->belongsTo('product_id', 'Products', 'product_id', array('alias' => 'Products'));
        $this->belongsTo('seller_id', 'Members', 'member_id', array('alias' => 'Members'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'order_detail';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return OrderDetail[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return OrderDetail
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    
    public function all($request){
        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['order_id'])) {
                $conditions[] = 'od.order_id IN ('.$p['order_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }
        $params = [
            'field' => 'od.*',
            'table' => 'order_detail od',
            'conditions' => $conditions,
//            'limit' => [0, 1],
//            'debug' => true
        ];
        
        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

}
