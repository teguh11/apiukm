<?php
// reference model
// https://docs.phalconphp.com/en/latest/reference/validation.html

use Phalcon\Validation\Validator\Email as Email;
use Phalcon\Validation\Validator\PresenceOf as Required;
use Phalcon\Validation\Validator\Uniqueness as Unique;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Security;
use Phalcon\Crypt;
//use Phalcon\Mvc\Model\Transaction\Manager;
// use ActiveRecord;

class Members extends Ukm
{

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $retypepassword;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var integer
     */
    public $auth_key;

    /**
     *
     * @var integer
     */
    public $member_store_id;
    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();
        $validator->add(
            'email', new Email([
              'model' =>  $this,
              'message' =>  'Format Email yang Anda masukkan salah'
            ]));
        $validator->add(
            'email', new Unique([
              'model' =>  $this,
              'message' =>  'Email sudah terdaftar, silahkan coba kembali'
            ])
        );

        $validator->add(
            'name', new Required([
              'model' =>  $this,
              'message' =>  'Nama tidak boleh kosong'
            ])
        );
        $validator->add(
            'password', new Required([
              'model' =>  $this,
              'messsage'  =>  'Password tidak boleh kosong'
            ])
        );
        // $validator->add(
        //     'password', new Confirmation([
        //       'model' =>  $this,
        //       'message' => 'Password tidak sama:'.$this->retypepassword.':'.$this->retypepassword,
        //       'with' => 'retypepassword'
        //     ])
        // );
        // $validator->add(
        //     'confirmPassword', new Required([
        //       // 'model' =>  $this,
        //       'messsage'  =>  'Retype Password tidak boleh kosong'
        //     ])
        // );
        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
//        $this->hasMany('member_id', 'MemberStore', 'member_id', array('alias' => 'MemberStore'));
//        $this->hasMany('member_id', 'OrderDetail', 'member_id', array('alias' => 'OrderDetail'));
//        $this->hasMany('member_id', 'OrderDetail', 'seller_id', array('alias' => 'OrderDetail'));
//        $this->hasMany('member_id', 'Orders', 'order_id', array('alias' => 'Orders'));
//        $this->hasMany('member_id', 'ProductCategories', 'member_id', array('alias' => 'ProductCategories'));
//        $this->hasMany('member_id', 'ProductItems', 'member_id', array('alias' => 'ProductItems'));
//        $this->hasMany('member_id', 'Products', 'member_id', array('alias' => 'Products'));
        $this->belongsTo('member_store_id', 'MemberStore', 'member_store_id',array('alias' => 'MemberStore'));
    }

    public function setRetypepassword(){
        return $this->retypepassword;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'members';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Members[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Members
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function all($request){
        $conditions = [];
        $p = $request->getQuery();

        $params = [
            'field' => 'm.member_id,m.name,m.email,m.active, m.created_at, m.updated_at',
            'table' => 'members m',
//            'order' => 'p.member_id',
//            'sort'  => 'DESC', //optional
            'conditions' => $conditions,
//            'join'  => ['LEFT JOIN member_store ms ON m.member_id = ms.member_id'],
//            'group' => ['pr.product_item_id'],
//            'limit' => [0, 1],
//            'debug' => true
        ];
        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function register($request){
        $security = new Security();
        $crypt = new Crypt();
        $memberStore = new MemberStore();
        $member = new Members();
        $now = date('Y-m-d H:i:s');
        $params = $request->getPost();
        $return = [];

        $manager = new \Phalcon\Mvc\Model\Transaction\Manager();
        $transaction = $manager->get();

        $memberStore->setTransaction($transaction);
        $memberStore->assign(array(
                'member_store_status'   =>  MemberStore::STATUS_NOT_ACTIVE
            ));
//
        if($memberStore->save() == false){
            $return = [
                'status'  =>  '2', // return FAILED for save member store
                'data'    =>  $memberStore,
                'message' =>  'Proses Gagal, Silahkan Coba Beberapa Saat Lagi'
            ];
        }
//
        if($memberStore->save()){ // if save to member store
           $member->setTransaction($transaction);
           $member->assign(array(
                'member_store_id'   =>  $memberStore->member_store_id,
                'name'  =>  $params['name'],
                'email' =>  $params['email'],
                'password'  =>  !empty($params['password']) ? $security->hash($params['password']) : '',
                // 'retypepassword'  =>  $params['retypepassword'],
                'type'  =>  0, // set default type member owner for registered new member,
                'active'  =>  1, // set status user active,
                'auth_key'  =>  md5($params['email'].$now)
            ));

            if($member->save()){ // if save to member
                $transaction->commit();
                $return = [
                    'status'  =>  '1', // return OK
                    'data'    =>  $params,
                    'message' =>  'Hanya beberapa langkah lagi Anda dapat menggunakan aplikasi ini. Silahkan cek email Anda di <strong>"'.$params['email'].'"</strong> untuk aktifasi akun Anda'
                ];
            }else{ // if failed save to member
                $error = [];
                foreach ($member->getMessages() as $i=>$message) {
                    $error[$i]['field'] = $message->getField();
                    $error[$i]['message'] = $message->getMessage();
                }

                $return = [
                    'status'  =>  '0', // return FAILED
                    'data'    =>  $error,
                    'message' =>  'Error'
                ];
            }
        }


        return $return;
    }

    public function updateData($request){

        $params = $request->getQuery();
        $where = 'member_id='.$params['member_id'];
        unset($params['member_id']);
        unset($params['_url']);
        unset($params['created_at']);
        $params['updated_at'] = date('Y-m-d H:i:s');
        $arrResult = ActiveRecord::update('members',$params, $where);
        return $arrResult;
    }

    public function deleteData($request){

        $params = $request->getQuery();
        $where = 'member_id='.$params['member_id'];
        $arrResult = ActiveRecord::delete('members', $where);
        return $arrResult;
    }

    public function auth($request){
        $query = "";
        $request = $request->getPost();
        if(isset($request['username']) && !empty($request['username'])){
            $query .= "email = '".$request['username']."'";
        }
        if(!empty($query)){
            $start_time = microtime(TRUE);

            $member =  Members::findFirst(["email = '".$request['username']."'"]);
            if(!empty($member)){
                $security = new Security();
                if($member->active == 0){ // check if status not actived
                  $data = [];
                  $message = "Silahkan Aktifkan Akun Anda Terlebih Dahulu";
                  $status = 0; // status return error
                }else{
                  if($security->checkHash($request['password'],$member->password)){
                      $data = (object) array_merge((array)$member, (array)$member->MemberStore) ;
                      $message = "Sukses";
                      $status = 1;
                  }else{
                      $data = [];
                      $message = "Password Salah";
                      $status = 0;
                  }
                }

            }else{
                $data = [];
                $message = "Username Tidak Terdaftar";
                $status = 0;
            }

            $end_time =microtime(TRUE);
            $time = $end_time - $start_time;

            $return = [
                'data'      =>$data,
                'message'   => $message,
                'status'    => $status,
                'time_execution' => $time
            ];
        }else{
            $return = [
              'data'      => $request,
              'message'   => 'Username Tidak Boleh Kosong',
              'status'    => 0,
              'time_execution' => microtime(TRUE)
            ];
        }
        return $return;
    }

}
