<?php

//alter table member_store drop foreign key member_id

class MemberStore extends Ukm
{

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     *
     * @var integer
     */
    public $member_store_id;

    /**
     *
     * @var string
     */
    public $member_store_name;

    /**
     *
     * @var string
     */
    public $member_store_logo;

    /**
     *
     * @var string
     */
    public $member_store_address;

    /**
     *
     * @var string
     */
    public $member_store_phone;

    /**
     *
     * @var string
     */
    public $member_store_lat;

    /**
     *
     * @var string
     */
    public $member_store_lng;

    /**
     *
     * @var integer
     */
    public $member_store_type;

    /**
     *
     * @var integer
     */
    public $member_store_status;

    /**
     *
     * @var integer
     */
    public $province_id;

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('member_store_id', 'Orders', 'order_id', array('alias' => 'Orders'));
        $this->hasMany('member_store_id', 'ProductCategories', 'member_store_id', array('alias' => 'ProductCategories'));
        $this->hasMany('member_store_id', 'ProductItems', 'member_store_id', array('alias' => 'ProductItems'));
        $this->hasMany('member_store_id', 'Products', 'member_store_id', array('alias' => 'Products'));
//        $this->belongsTo('member_id', 'Members', 'member_id', array('alias' => 'Members'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'member_store';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MemberStore[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MemberStore
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeValidationOnCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeValidationOnUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function all($request){
        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
//            if(!empty($p['product_id'])) {
//                $conditions[] = 'p.product_id IN ('.$p['product_id'].')';
//            }
//            if(!empty($p['product_active'])) {
//                $conditions[] = 'p.product_active = '.$p['product_active'].'';
//            }
//            if(!empty($p['product_title'])) {
//                $conditions[] = 'p.product_title like '.$p['product_title'].'';
//            }
//            $conditions = join(' AND ', $conditions);
        }

        $params = [
            'field' => 'm.member_id,m.name,m.email,m.active, m.created_at, m.updated_at,ms.*',
            'table' => 'members m',
//            'order' => 'p.member_id',
//            'sort'  => 'DESC', //optional
            'conditions' => $conditions,
            'join'  => ['LEFT JOIN member_store ms ON m.member_id = ms.member_id'],
//            'group' => ['pr.product_item_id'],
//            'limit' => [0, 1],
//            'debug' => true
        ];
        $arrResult = ActiveRecord::fetchAll($params);
//        die('dsadas');
        return $arrResult;
    }

    public function add($req){
        $memberstore = new MemberStore();
//        $memberstore->member_id = $req->getPost("member_id");
        $memberstore->member_store_name = $req->getPost("member_store_name");
        $memberstore->member_store_address = $req->getPost("member_store_address");
        $memberstore->member_store_phone = $req->getPost("member_store_phone");
        $memberstore->member_store_type = $req->getPost("member_store_type");
        $memberstore->member_store_status = $req->getPost("member_store_status");
        $memberstore->province_id = $req->getPost("province_id");
        $memberstore->city_id = $req->getPost("city_id");
        $memberstore->member_store_logo = $req->getPost("member_store_logo");
        $memberstore->member_store_lat = $req->getPost("member_store_lat");
        $memberstore->member_store_lng = $req->getPost("member_store_lng");
        if ($memberstore->save() == false) {
            $return = $this->returnValue("error");
        } else {
            $return = $this->returnValue("success");
        }
        return $return;
    }

    public function updateData($id, $req){
        $memberstore = MemberStore::findFirst("member_store_id=".$id);
        if(!empty($memberstore)){
            $memberstore->member_store_name = $req->getPost("member_store_name");
            $memberstore->member_store_address = $req->getPost("member_store_address");
            $memberstore->member_store_phone = $req->getPost("member_store_phone");
            $memberstore->member_store_type = $req->getPost("member_store_type");
            $memberstore->member_store_status = $req->getPost("member_store_status");
            $memberstore->province_id = $req->getPost("province_id");
            $memberstore->city_id = $req->getPost("city_id");
            $memberstore->member_store_logo = $req->getPost("member_store_logo");
            $memberstore->member_store_lat = $req->getPost("member_store_lat");
            $memberstore->member_store_lng = $req->getPost("member_store_lng");
            if(!$memberstore->save()){
                $return = $this->returnValue("error");
            }else{
                $return = $this->returnValue("success");
            }
        }else{
            $return = $this->returnValue("error", "data tidak ditemukan");
        }

        return $return;

    }

}
