<?php
//tes
/**
 * Created by PhpStorm.
 * User: iwansusanto
 * Date: 4/19/16
 * Time: 10:35 PM
 */
class Ukm extends \Phalcon\Mvc\Model
{
    public function beforeValidationOnCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeValidationOnUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function returnValue($type="success|error",$message="message to display",$data=[]){
        if($type == "success"){
            $return = [
                'message'   => (!empty($message)?$message:"success query"),
                'status'    => 1,
                'time_execution' => microtime(TRUE),
                'data'      => $data,
            ];
        }else{
            $errordata = [];
            if(!empty($data)){
                foreach($data as $id=>$val){
                    $errordata[$id]['field'] = $val->getField();
                    $errordata[$id]['message'] = $val->getMessage();
                }
            }
            $return = [
                'message'   => (!empty($message)?$message:"error query"),
                'status'    => 0,
                'time_execution' => microtime(TRUE),
                'data'      => $errordata,
            ];
        }
        return $return;
    }

    public function statusUkm() {
        return [
            'order' => [
                    'orders' => ['label'=>'Orders','value'=>0],
                    'masak' => ['label'=>'Masak','value'=>1],
                    'matang' => ['label'=>'Matang','value'=>2],
                    'antar' => ['label'=>'Antar','value'=>3],
                    'santap' => ['label'=>'Santap','value'=>4],
                    'selesai' => ['label'=>'Selesai','value'=>5],
                ],
            'paybill' => [
                    ['id'=>1,'name'=>'Tunai','selected'=> true],
                    ['id'=>2,'name'=>'Kartu','selected'=> false],
                    ['id'=>3,'name'=>'Voucher','selected'=> false],
                ]
            ];
            
    }
    public function struk($request) {
        $p = $request->getQuery();
        
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'o.member_id IN ('.$p['member_id'].')';
            }
            if(!empty($p['member_store_id'])) {
                $conditions[] = 'o.member_store_id IN ('.$p['member_store_id'].')';
            }
            if(!empty($p['order_id'])) {
                $conditions[] = 'od.order_id IN ('.$p['order_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }

        $params = [
            'field' => 'od.order_price,od.order_qty,pi.product_item_name,pi.product_item_price,p.product_title,t.total,t.nominal,t.kembali,t.create_at,t.order_id',
            'table' => 'orders o',
            'conditions' => $conditions,
            'join'  => [
                'LEFT JOIN order_detail od ON od.order_id = o.order_id',
                'LEFT JOIN product_items pi ON pi.product_item_id = od.product_item_id',
                'LEFT JOIN products p ON p.product_id = od.product_id',
                'LEFT JOIN transaksi t ON t.order_id = o.order_id'
            ],
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }
}
