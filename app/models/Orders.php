<?php
// use ActiveRecord;
class Orders extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $order_id;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $member_store_id;

    /**
     *
     * @var string
     */
    public $order_transaction_number;

    /**
     *
     * @var string
     */
    public $order_transaction_evidence;

    /**
     *
     * @var integer
     */
    public $order_qty;

    /**
     *
     * @var integer
     */
    public $order_price;

    /**
     *
     * @var integer
     */
    public $order_status;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $update_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('order_id', 'OrderDetail', 'order_id', array('alias' => 'OrderDetail'));
        $this->belongsTo('order_id', 'Members', 'member_id', array('alias' => 'Members'));
        $this->belongsTo('order_id', 'MemberStore', 'member_store_id', array('alias' => 'MemberStore'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'orders';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Orders[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Orders
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function all($request){
        $mUkm = new Ukm();
        $conditions = [];
        $conditions[] = 'o.order_status!='.$mUkm->statusUkm()['order']['selesai']['value'];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'o.member_id IN ('.$p['member_id'].')';
            }
            if(!empty($p['member_store_id'])) {
                $conditions[] = 'o.member_store_id IN ('.$p['member_store_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }
        $params = [
            'field' => 'o.*',
            'table' => 'orders o',
            'conditions' => $conditions,
//            'limit' => [0, 1],
//            'debug' => true
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function detail($request){
        $conditions = [];
        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'o.member_id IN ('.$p['member_id'].')';
            }
            if(!empty($p['member_store_id'])) {
                $conditions[] = 'o.member_store_id IN ('.$p['member_store_id'].')';
            }
            if(!empty($p['order_id'])) {
                $conditions[] = 'od.order_id IN ('.$p['order_id'].')';
            }
            $conditions = join(' AND ', $conditions);
        }

        $params = [
            'field' => 'o.*,od.*,pi.product_item_name,pi.product_item_price,p.product_title',
            'table' => 'orders o',
            'conditions' => $conditions,
            'join'  => [
                'LEFT JOIN order_detail od ON od.order_id = o.order_id',
                'LEFT JOIN product_items pi ON pi.product_item_id = od.product_item_id',
                'LEFT JOIN products p ON p.product_id = od.product_id'
            ],
//            'limit' => [0, 1],
//            'debug' => true
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function add($request){
        $params = $request->getPost();
        return $this->saveOrder($params,'add');
    }

    public function updateData($request){
        $params = $request->getPost();
        if(isset($params['action']) && $params['action']=='bayar') {
            return $this->bayar($params);
        } else if(!isset($params['orders'])){ // update status
            return $this->updateStatus($params);
        } else {
            return $this->saveOrder($params,'update');
        }
    }

    public function updateStatus($params) {
        $id = $params['id'];
        unset($params['id']);
        return ActiveRecord::update('orders',$params, 'order_id='.$id);
    }
    public function deleteData($request) {
        $params = $request->getPost();
        $id = $params['order_id'];
        $del_order = ActiveRecord::remove('orders', 'order_id='.$id);
        $del_order_detail = ActiveRecord::remove('order_detail', 'order_id='.$id);
        return ($del_order && $del_order_detail ? 1 : 0);
    }
    public function getArrayById() {
        $params = [
            'field' => 'pi.*',
            'table' => 'product_items pi',
            'conditions' => $conditions,
//            'join'  => ['RIGHT JOIN order_detail od ON od.order_id = o.order_id'],
//            'limit' => [0, 1],
//            'debug' => true
        ];
        $arrResult = ActiveRecord::fetchAll($params);
        $r = '';
        foreach ($arrResult['rows'] as $key => $value) {
            $r[$value['product_item_id']] = $value;
        }
        return $r;
    }
    public function saveOrder($params, $action) {
        $result = 0;
        $id = $params['order_id'];
        $order_detail = json_decode($params['orders']);
        unset($params['orders']);
        unset($params['order_id']);
        unset($params['nama']);
        unset($params['telpon']);

        $order_qty = 0; $sub_harga = 0;
        $product_all = $this->getArrayById();
        foreach ($order_detail as $key => $value) {
            $d = $product_all[$value->product_item_id];
            $order_qty += $value->order_qty;
            $sub_harga += $value->order_qty * $d['product_item_price'];
        }

//        $params['order_transaction_number'] = '';
        $params['order_transaction_evidence'] = $params['order_transaction_number'];
        $params['order_qty'] = $order_qty;
        $params['order_price'] = $sub_harga;
        $params['order_status'] = 0;
        if($action=='add'){
            $params['created_at'] = date('Y-m-d H:i:s');
            $arrResult = ActiveRecord::insert('orders',$params);
        }
        if($action=='update'){
            $params['update_at'] = date('Y-m-d H:i:s');
            $arrResult = ActiveRecord::update('orders',$params, 'order_id='.$id);
        }
        if(is_numeric($arrResult['status'])) {

            if($action=='update'){
                ActiveRecord::remove('order_detail', 'order_id='.$id);
            }

            $d = '';
            foreach ($order_detail as $key => $value) {
                $det = '';
                $d = $product_all[$value->product_item_id];
                $det = [
                    'order_id'=>($action=='add'?$arrResult:$id),
                    'order_price'=> $value->sub_harga,
                    'order_use_point'=> 0,
                    'order_point'=> 0,
                    'order_qty'=> $value->order_qty,
                    'order_resi'=> $params['order_transaction_number'],
                    'member_id'=> $params['member_id'],
                    'seller_id'=> 1,
                    'product_id'=> $d['product_id'],
                    'product_item_id'=> $value->product_item_id,
                    'product_category_id'=> $d['product_category_id'],
                    'product_category'=> $d['product_item_name'],
                    'product_category_parent'=> $d['product_item_name'],
                    'created_at'=> date('Y-m-d H:i:s'),
                ];
                ActiveRecord::insert('order_detail',$det);
            }
            $result = $arrResult;
        }
        return $result;
    }

    public function bayar($params) {
        $id = $params['id'];
        $nominal_transaksi = 0;
        
        if(isset($params["bayar"]) && !empty($params["bayar"])){ // tunai
            $data['nominal_tunai']=$params["bayar"];
            $nominal_transaksi += (int)$params["bayar"];
        }
        
        if(isset($params["bayar_card"]) && !empty($params["bayar_card"])){ // kartu
            $data['no_card'] = $params["no_card"];
            $data['mid'] = $params["mid"];
            $data['tid'] = $params["tid"];
            $data['nominal_card'] = $params["bayar_card"];
            $nominal_transaksi += (int)$params["bayar_card"];
        }
        if(isset($params["bayar_voucher"]) && !empty($params["bayar_voucher"])){ // voucher
            $data['no_voucher'] = $params["no_voucher"];
            $data['expired_voucher'] = $params["expired_voucher"];
            $data['nominal_voucher'] = $params["bayar_voucher"];
            $nominal_transaksi += (int)$params["bayar_voucher"];
        }
        
        if(!empty($nominal_transaksi)) {
            $data['order_id'] = $params['id'];
            $data['total'] = $params['total_order'];
            $data['nominal'] = $nominal_transaksi;
            $data['kembali'] = $params['kembali'];

            ActiveRecord::insert('transaksi',$data);
            
            // kurangin stock
            $arrResult = ActiveRecord::fetchAll([
                    'field' => ['od.product_item_id', 'od.order_qty', 'pi.product_item_current_stock', 'pi.product_item_stock'],
                    'table' => 'order_detail od',
                    'join' =>[
                        'JOIN product_items pi ON pi.product_item_id = od.product_item_id'
                    ],
                    'conditions' => 'od.order_id='.$id,
            ]);

            foreach ($arrResult['rows'] as $key => $value) {
                $dataif = ($value['product_item_current_stock'] > 0 ? $value['product_item_current_stock'] - $value['order_qty'] : $value['product_item_stock'] - $value['order_qty']);
                ActiveRecord::update('product_items',[
                        "product_item_current_stock"=>$dataif,
                ], 'product_item_id='.$value['product_item_id']);
            }
            
            
            $result = ActiveRecord::update('orders',[
                "order_status"=>$params["order_status"],
            ], 'order_id='.$id);
        }
        
        if(isset($result) && is_numeric($result['status'])){
            return [
                'status' => 1,
                'message' => 'update berhasil',
            ];
        }else{
            return [
                'status' => 0,
                'message' => 'update gagal',
            ];
        }
        
    }

    public function report($request){
        $conditions = [];
        $order = '';
        $group = [];
        $field = [];
        $join =  [];
        $field[] = 'o.*, count(o.order_id) as total_order';
        $field[] = 'sum(o.order_price) as order_price';



        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'o.member_id IN ('.$p['member_id'].')';
            }

            if(!empty($p['member_store_id'])) {
                $conditions[] = 'o.member_store_id IN ('.$p['member_store_id'].')';
            }

            //untuk report penjualan per product
            if(!empty($p['product_id'])) {
                $field[] = 'od.product_id';
                $join[]  = 'INNER JOIN order_detail od ON o.order_id = od.order_id';
                $conditions[] = 'od.product_id ='.$p['product_id'].'';
            }

            //report based on date range
            //date start
            if(
                (isset($p['ds']) && !empty($p['ds'])) &&
                (isset($p['de']) && !empty($p['de']))
            ){
                 $conditions[] = ' date(o.created_at) >= \''.$p['ds'].'\' AND date(o.created_at) <= \''.$p['de'].'\'';
            }else{
                if(isset($p['ds']) && !empty($p['ds']))
                {
                  preg_match("/^\d{4}\-\d{2}$/", $p['ds'], $match);
                  if(!empty($match[0]))
                  {
                      $m = explode('-', $match[0]);
                      $conditions[] = ' YEAR(o.created_at) = \''.$m[0].'\'';
                      $conditions[] = ' MONTH(o.created_at) = \''.$m[1].'\'';
                  }else{
                      $conditions[] = ' date(o.created_at) = \''.$p['ds'].'\'';
                  }
                }

                //date end
                if(isset($p['de']) && !empty($p['de']))
                {
                  $conditions[] = ' date(o.created_at) =\''.$p['de'].'\'';
                }

                if(isset($p['dsg']) && !empty($p['dsg']))
                {
                  $conditions[] = ' date(o.created_at) >= \''.$p['dsg'].'\'';
                }
            }

            $conditions = join(' AND ', $conditions);
        }

        //monthly report atau daily report
        if(isset($p['r']))
        {
            if($p['r'] == 'day'){
              $group[] = 'DATE(created_at)';
            }

            if($p['r'] == 'month'){
              $group[] = 'MONTH(created_at)';

            }

            $group = join('  ', $group);
        }

        //for detail order, jangan digrouping, dibuat per order ID.
        if(isset($p['order_type']))
        {
            if($p['order_type'] == 'detail'){
              $group[] = 'o.order_id';
            }

            $group = join('  ', $group);
        }

        $field = join(', ', $field);


        if(!empty($join))
          $join = join(' ', $join);


        $order = 'created_at ASC';
        $params = [
            'field' => $field,
            'table' => 'orders o',
            'conditions' => $conditions,
            'group' => $group,
            'join' => $join,
            'order' => $order,
            'debug' => false
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function report_product($request){
        $conditions = [];
        $order = '';
        $group = [];


        $p = $request->getQuery();
        if(!empty($p)) {
            // if(!empty($p['member_id'])) {
            //     $conditions[] = 'o.member_id IN ('.$p['member_id'].')';
            // }
            //
            // if(!empty($p['member_store_id'])) {
            //     $conditions[] = 'o.member_store_id IN ('.$p['member_store_id'].')';
            // }

            //report based on date range
            //date start
            if(
                (isset($p['ds']) && !empty($p['ds'])) &&
                (isset($p['de']) && !empty($p['de']))
            ){
                 $conditions[] = ' date(o.created_at) >= \''.$p['ds'].'\' AND date(o.created_at) <= \''.$p['de'].'\'';
            }else{
                if(isset($p['ds']) && !empty($p['ds']))
                {
                  preg_match("/^\d{4}\-\d{2}$/", $p['ds'], $match);
                  if(!empty($match[0]))
                  {
                      $m = explode('-', $match[0]);
                      $conditions[] = ' YEAR(o.created_at) = \''.$m[0].'\'';
                      $conditions[] = ' MONTH(o.created_at) = \''.$m[1].'\'';
                  }else{
                      $conditions[] = ' date(o.created_at) = \''.$p['ds'].'\'';
                  }
                }

                //date end
                if(isset($p['de']) && !empty($p['de']))
                {
                  $conditions[] = ' date(o.created_at) =\''.$p['de'].'\'';
                }

                if(isset($p['dsg']) && !empty($p['dsg']))
                {
                  $conditions[] = ' date(o.created_at) >= \''.$p['dsg'].'\'';
                }
            }

            $conditions = join(' AND ', $conditions);
        }

        //monthly report atau daily report
        if(isset($p['r']))
        {

            $group[] = 'pi.product_item_id';

            if($p['r'] == 'day'){
              $group[] = 'DATE(o.created_at)';
            }

            if($p['r'] == 'month'){
              $group[] = 'MONTH(o.created_at)';
            }



            $group = join(', ', $group);
        }

        //for detail order, jangan digrouping, dibuat per order ID.
        if(isset($p['order_type']))
        {
            if($p['order_type'] == 'detail'){
              $group[] = 'o.order_id';
            }

            $group = join('  ', $group);
        }
//count(o.order_id) as total_order,
        $order = 'o.created_at ASC';
        $params = [
            'field' => 'p.product_title, p.product_title_slug, pi.product_item_name, p.product_id, o.*, count(o.order_id) as total_order, sum(o.order_price) as order_price',
            'table' => 'order_detail o',
            'conditions' => $conditions,
            'join'  => [
                'LEFT JOIN product_items pi ON pi.product_item_id = o.product_item_id',
                'LEFT JOIN products p ON p.product_id = o.product_id'
            ],
            'group' => $group,
            'order' => $order,
            'debug' => false
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

    public function report_dashboard($request){
        $conditions = [];
        $order = '';
        $group = [];
        $field = [];
        $join =  [];
        $field[] = 'o.*, count(o.order_id) as total_order';
        $field[] = 'sum(o.order_price) as order_price';



        $p = $request->getQuery();
        if(!empty($p)) {
            if(!empty($p['member_id'])) {
                $conditions[] = 'o.member_id IN ('.$p['member_id'].')';
            }

            if(!empty($p['member_store_id'])) {
                $conditions[] = 'o.member_store_id IN ('.$p['member_store_id'].')';
            }

            //untuk report penjualan per product
            if(!empty($p['product_id'])) {
                $field[] = 'od.product_id';
                $join[]  = 'INNER JOIN order_detail od ON o.order_id = od.order_id';
                $conditions[] = 'od.product_id ='.$p['product_id'].'';
            }

            //report based on date range
            //date start
            if(
                (isset($p['ds']) && !empty($p['ds'])) &&
                (isset($p['de']) && !empty($p['de']))
            ){
                 $conditions[] = ' date(o.created_at) >= \''.$p['ds'].'\' AND date(o.created_at) <= \''.$p['de'].'\'';
            }else{
                if(isset($p['ds']) && !empty($p['ds']))
                {
                  preg_match("/^\d{4}\-\d{2}$/", $p['ds'], $match);
                  if(!empty($match[0]))
                  {
                      $m = explode('-', $match[0]);
                      $conditions[] = ' YEAR(o.created_at) = \''.$m[0].'\'';
                      $conditions[] = ' MONTH(o.created_at) = \''.$m[1].'\'';
                  }else{
                      $conditions[] = ' date(o.created_at) = \''.$p['ds'].'\'';

                  }
                }

                //date end
                if(isset($p['de']) && !empty($p['de']))
                {
                  $conditions[] = ' date(o.created_at) =\''.$p['de'].'\'';
                }

                if(isset($p['dsg']) && !empty($p['dsg']))
                {
                  $conditions[] = ' date(o.created_at) >= \''.$p['dsg'].'\'';
                }
            }


        }

        //monthly report atau daily report
        if(isset($p['r']))
        {
            if($p['r'] == 'day'){
              $group[] = 'DATE(created_at)';
              $conditions[] = ' date(o.created_at) = CURDATE()';

            }

            if($p['r'] == 'month'){
              $group[] = 'MONTH(created_at)';
              $conditions[] = ' MONTH(o.created_at) = MONTH(NOW())';
            }

            $group = join('  ', $group);
        }

        //for detail order, jangan digrouping, dibuat per order ID.
        if(isset($p['order_type']))
        {
            if($p['order_type'] == 'detail'){
              $group[] = 'o.order_id';
            }

            $group = join('  ', $group);
        }

        $field = join(', ', $field);




        if(!empty($conditions))
          $conditions = join(' AND ', $conditions);

        if(!empty($join))
          $join = join(' ', $join);


        $order = 'created_at ASC';
        $params = [
            'field' => $field,
            'table' => 'orders o',
            'conditions' => $conditions,
            'group' => $group,
            'join' => $join,
            'order' => $order,
            'debug' => FALSE
        ];

        $arrResult = ActiveRecord::fetchAll($params);
        return $arrResult;
    }

}
