<?php
//$config = include __DIR__ . "/config/config.php";

class ProductItemMedia extends Ukm
{

    /**
     *
     * @var integer
     */
    public $product_item_media_id;

    /**
     *
     * @var integer
     */
    public $product_item_id;

    /**
     *
     * @var string
     */
    public $media_title;

    /**
     *
     * @var string
     */
    public $media_path;

    /**
     *
     * @var integer
     */
    public $media_type;

    /**
     *
     * @var integer
     */
    public $media_active;

    /**
     *
     * @var integer
     */
    public $media_primary;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('product_item_id', 'ProductItems', 'product_item_id', array('alias' => 'ProductItems'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_item_media';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductItemMedia[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductItemMedia
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function add($request){
        $return = [];

        if($request->hasFiles() == true){
            foreach($request->getUploadedFiles() as $file) {
//                SAVE FILE TO FOLDER
                $directory_path = $request->getPost('member_id')."/".$request->getPost('member_store_id');
                $root_path = $this->getDI()->getConfig()->application->imageDir.$directory_path;
                if(!file_exists($root_path)){
                    mkdir($root_path, 0755, true);
                }
                $file->moveTo($root_path."/".$file->getName());
                if(!empty($request->getPost("product_item_media_id"))){
//                    update
                    $value = [
                        'product_item_media_id' => $request->getPost('product_item_media_id'),
                        'product_item_id'   => $request->getPost('product_item_id'),
                        'media_title'       => 'test',
                        'media_path'        => $directory_path."/".$file->getName(),
                        'media_type'        => 1,
                        'media_active'      => 1,
                        'media_primary'     => 0,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ];
                    $this->assign($value);
                    if($this->save()){
                        $return = [
                            'status' => 1,
                            'lastInsertId' => $this->product_item_media_id,
                            'image_url' => $this->getDI()->getConfig()->application->imageUrl.$directory_path."/".$file->getName(),
                            'message' => 'success',
                        ];
                    }else{
                        $errorMsg = [];
                        foreach ($this->getMessages() as $id => $message) {
                            $errorMsg[$id]['field'] = $message->getField();
                            $errorMsg[$id]['message'] = $message->getMessage();
                            $errorMsg[$id]['type'] = $message->getType();
                        }
                        $return = [
                            'status' => 0,
                            'data' => [],
                            'message' => $errorMsg,
                        ];
                    }

                }else{
//                    insert
                    $value = [
                        'product_item_id'   => $request->getPost('product_item_id'),
                        'media_title'       => 'test',
                        'media_path'        => $directory_path."/".$file->getName(),
                        'media_type'        => 1,
                        'media_active'      => 1,
                        'media_primary'     => 0,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ];
                    $this->assign($value);
                    if($this->save()){
                        $return = [
                            'status' => 1,
                            'lastInsertId' => $this->product_item_media_id,
                            'image_url' => $this->getDI()->getConfig()->application->imageUrl.$directory_path."/".$file->getName(),
                            'message' => 'success',
                        ];
                    }else{
                        $return = [
                            'status' => 0,
                            'data' => [],
                            'message' => 'failed',
                        ];
                    }
                }

            }

        }else{
            $return = [
                'status' => 0,
                'message' => 'failed',
            ];
        }
        return $return;
    }
}
