<?php
//tes
/**
 * Created by PhpStorm.
 * User: iwansusanto
 * Date: 3/22/16
 * Time: 6:19 PM
 */
//error_reporting(0);
 error_reporting(E_ALL);
use Phalcon\Mvc\Micro;
// use Components\ActiveRecord;

$config = include __DIR__ . "/config/config.php";
include __DIR__ . "/config/services.php";
include __DIR__ . "/config/loader.php";
include __DIR__ . "/components/ActiveRecord.php";



$app = new Micro($di);
$request = new \Phalcon\Http\Request();
$app->before(function() use ($app) {

  $origin = $app->request->getHeader("ORIGIN") ? $app->request->getHeader("ORIGIN") : '*';
  $app->response->setHeader("Access-Control-Allow-Origin", $origin)
                ->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
                ->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Access-Token')
                ->setHeader("Access-Control-Allow-Credentials", true);

    if($app->router->getMatchedRoute()->getPattern() == "/auth/member"){
       return true;
    }
    return true;
});


$app->get('/statusukm/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter
    $ukm = new Ukm();
    $return = $ukm->statusUkm($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/print/struk', function() use($app, $request){ // optional, bisa all, bisa kasih parameter
    $ukm = new Ukm();
    $return = $ukm->struk($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

// model meja
$app->get('/meja/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter
    $meja = new Meja();
    $return = $meja->all($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});
$app->post('/meja/add', function() use ($app){
    $meja = new Meja();
    $return = $meja->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});
$app->post('/meja/update', function() use ($app){
    $meja = new Meja();
    $return = $meja->updateData($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

/**
 * REGISTER NEW MEMBER
 */
$app->post('/auth/member/register', function() use ($app, $request){
    $members    = new Members();
    $return = $members->register($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    if($return['status'] < 1){
        $app->response->setStatusCode(400, "Bad Request")->send();
    }else{
        $app->response->setStatusCode(200, "OK")->send();
    }
    // return $app->response;
});

$app->post('/auth/member/update', function() use ($app, $request){
    $members    = new Members();
    $return = $members->updateData($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/auth/member/delete', function() use ($app, $request){
    $members    = new Members();
    $return = $members->deleteData($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});


/**
 * AUTH MEMBER VALIDATION
 */
$app->post('/auth/member/login', function() use ($app){
    // $request = $app->request->getJsonRawBody();

    $members    = new Members();
    $return = $members->auth($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/members/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter
    $members = new Members();
    $return = $members->all($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

/**
 * START MEMBERSTORE
 */

$app->get('/memberstore/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter
    $memberstore = new MemberStore();
    $return = $memberstore->all($request);
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/memberstore/add', function() use($app){
    $memberstore = new MemberStore();
    $return = $memberstore->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/memberstore/update/{id:[0-9]+}', function ($id) use ($app) {

    $memberStore = new MemberStore();
    $return = $memberStore->updateData($id, $app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});
/**
 * END MEMBERSTORE
*/


/**
 * START PRODUCT CATEGORIES
 */
$app->get('/productcategories/list', function() use($app, $request){
    $productCategories = new ProductCategories();
    $return = $productCategories->all($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});
$app->post('/productcategories/add', function() use($app, $request){
    $productCategories = new ProductCategories();
    $return = $productCategories->add($request);
//    $return = ["test", "test"];
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/productcategories/update', function() use ($app, $request){
    $productCategory    = new ProductCategories();
    $return = $productCategory->updateData($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});
$app->post('/productcategories/delete', function() use ($app, $request){
    $productCategory    = new ProductCategories();
    $return = $productCategory->deleteData($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});



/**
 * END PRODUCT CATEGORIES
*/

/**
 * START PRODUCT
*/

$app->get('/product/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $product = new Products();
    $return = $product->all($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/product/add', function() use($app){
    $product = new Products();
    $return = $product->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

//$app->post('/product/update', function() use ($app, $request){
//    $product = new ProductItems();
//    $return = $product->updateData($request);
//    $app->response->setContentType("application/json")->sendHeaders();
//    $app->response->setJsonContent($return);
//    return $app->response;
//});

$app->get('/product/detail', function() use ($app){
    $product = new Products();
    $return = $product->detail($app->request);
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
//    if($return['status'] < 1){
//        $app->response->setStatusCode(400, "Bad Request")->send();
//    }else{
//        $app->response->setStatusCode(200, "OK")->send();
//    }
    return $app->response;
});


/**
 * END PRODUCT
*/

/**
 * START PRODUCT ITEM
*/
$app->post('/productitem/add', function() use($app){
    $productItems = new ProductItems();
    $return = $productItems->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/productitem/update/{id:[0-9]+}', function($id) use($app){
    $productItems = new ProductItems();
    $return = $productItems->updateData($id, $app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;

});
/**
 * END PRODUCT ITEM
 */


/**
 * START PRODUCT MEDIA
*/
$app->post('/productitemmedia/add', function() use($app, $config){
    $productItemMedia = new ProductItemMedia();
    $return = $productItemMedia->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    if($return['status'] < 1){
        $app->response->setStatusCode(400, "Bad Request")->send();
    }else{
        $app->response->setStatusCode(200, "OK")->send();
    }
});
$app->post('/productitemmedia/update/{id:[0-9]+}', function($id) use($app, $config){
    $productItemMedia = new ProductItemMedia();
    $return = $productItemMedia->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    if($return['status'] < 1){
        $app->response->setStatusCode(400, "Bad Request")->send();
    }else{
        $app->response->setStatusCode(200, "OK")->send();
    }
});



$app->get('/orders/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter
    $orders = new Orders();
    $return = $orders->all($request);
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/orders/listdetail', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->detail($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/orders/report', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->report($request);
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/orders/report_product', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->report_product($request);
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/orders/report_dashboard', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->report_dashboard($request);
    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});


$app->post('/orders/add', function() use($app){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->add($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/orders/update', function() use($app){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->updateData($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->post('/orders/delete', function() use($app){ // optional, bisa all, bisa kasih parameter

    $orders = new Orders();
    $return = $orders->deleteData($app->request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});

$app->get('/orderdetail/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $order_detail = new OrderDetail();
    $return = $order_detail->all($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});



$app->get('/barang/list', function() use($app, $request){ // optional, bisa all, bisa kasih parameter

    $product = new Products();
    $return = $product->allBarang($request);

    $app->response->setContentType("application/json")->sendHeaders();
    $app->response->setJsonContent($return);
    return $app->response;
});




// NOT FOUND VALIDATION
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'This is crazy, but this page was not found!';
});

$app->handle();
