<?php

// namespace Components\ActiveRecord;
use Phalcon\DI;
use Phalcon\Cache\Backend\File as BackFile;
use Phalcon\Cache\Frontend\Output as FrontOutput;

class ActiveRecord
{


    private static function db()
    {
        return DI::getDefault()->getDb();
    }

    private static function opt($param){

//============example full params
//        $param = [
//            'field' => 'p.*, pr.*',
//            'table' => 'products p',
//            'order' => 'p.product_id',
//            'sort'  => 'DESC', //optional
//            'conditions' => [
//                'p.product_id IN (?,?,?)',
//                'p.product_title LIKE ?'
//            ],
//            'join'  => ['INNER JOIN product_items pr ON p.product_id = pr.product_id'],
//            'group' => ['pr.product_item_id'],
//            'limit' => [0, 1],
//            'bind'  =>[293,294,295,'%asdf%'],
//            'debug' => true
//            'fetch' => 'one' //one || all
//        ];
//================================

        $table = isset($param['table']) ? $param['table'] : '';

        $having = '';
        if(isset($param['having']) && !empty($param['having'])) {
            if (is_array($param['having'])) {
                if(count($param['having']) == 1){
                    $having = ' HAVING '. $param['having'][0];
                }else{
                    $having = ' HAVING '. implode(' , ', $param['having']);
                }
            }else{
                $having = ' HAVING '. $param['having'];
            }
        }

        $field = '*';
        if(isset($param['field']) && !empty($param['field'])) {
            if (is_array($param['field'])) {
                if(count($param['field']) == 1){
                    $field = $param['field'][0];
                }else{
                    $field = implode(' , ', $param['field']);
                }
            }else{
                $field = $param['field'];
            }
        }

        $where = '';
        $bind = '';
        if(isset($param['conditions']) && !empty($param['conditions'])){
            if(is_array($param['conditions'])){
                if(count($param['conditions']) == 1){
                    $where = ' WHERE '. $param['conditions'][0];
                }else{
                    $where = ' WHERE '. implode(' AND ', $param['conditions']);
                }
            }else{
                $where = ' WHERE '. $param['conditions'];
            }
            $bind = (isset($param['bind']) && !empty($param['bind'])) ? $param['bind'] : '';
        }

        $join = '';
        if(isset($param['join']) && !empty($param['join'])) {
            if (is_array($param['join'])) {
                if(count($param['join']) == 1){
                    $join = $param['join'][0];
                }else{
                    $join = implode(' ', $param['join']);
                }
            }else{
                $join = $param['join'];
            }
        }

        $group = '';
        if(isset($param['group']) && !empty($param['group'])) {
            if (is_array($param['group'])) {
                if(count($param['group']) == 1){
                    $group = ' GROUP BY '. $param['group'][0];
                }else{
                    $group = ' GROUP BY '. implode(' , ', $param['group']);
                }
            }else{
                $group = ' GROUP BY '. $param['group'];
            }
        }

        $order = '';
        if(isset($param['order']) && !empty($param['order'])){
            $sort = (isset($param['sort']) && !empty($param['sort'])) ? $param['sort'] : '';
            $order = ' ORDER BY '. $param['order'].' '.$sort;
        }

        $limit = '';
        if(isset($param['limit']) && !empty($param['limit'])) {
            if (is_array($param['limit'])) {
                if(count($param['limit']) == 1){
                    $limit = ' LIMIT '. $param['limit'][0];
                }else{
                    $limit = ' LIMIT '. implode(' , ', $param['limit']);
                }
            }else{
                $limit = ' LIMIT '. $param['limit'];
            }
        }

        $sql = "SELECT {$having} {$field} FROM {$table} {$join} {$where} {$group} {$order} {$limit}";
        $sqlCount = "SELECT {$having} {$field} FROM {$table} {$join} {$where} {$group} {$order}";

        if(isset($param['debug']) && ($param['debug'] == true)) {
            echo '<div style="width:70%;border:1px solid #ccc;background-color:#eee;padding:5px;font-size:14px;line-height:1.5em;">';
            echo $sql;
            echo '<br />BIND : ';
            print_r($bind);
            echo '<br />PARAM : ';
            print_r($param);
            echo '</di>';
            die;
        }

        $param['logs'] = true;
        if(isset($param['logs']) && ($param['logs'] == true))
        {
            //error_log(print_r($sql, true)."\n". print_r($param, true). "\n". print_r($bind, true)."\n\n\n", 3, "/var/www/senaautopart/logs/query.log");
        }

        $res = null;
        if(isset($param['cache']) && $param['cache'] != false)
        {
            $config = DI::getDefault()->getConfig();
            // Create an Output frontend. Cache the files for 2 days
            $lifetime = (isset($param['cache']['lifetime']) && !empty($param['cache']['lifetime'])) ? $param['cache']['lifetime'] : $config->cache_lifetime_default;
            $cache_key = "{$table}#{$where} {$group} {$order} {$limit}";
            $key = (isset($param['cache']['key']) && !empty($param['cache']['key'])) ? $param['cache']['key'] : strtolower(preg_replace("/\s+/", "_", $cache_key));

            $frontCache = new FrontOutput(["lifetime" => $lifetime]);
            $cache = new BackFile($frontCache, ["cacheDir" => $config->cache_dir]);
            $get_key = $cache->get($key);
            $res = json_decode($get_key, true);
        }

        if ($res === null) {

            $res['rows'] = self::db()->fetchAll($sql, \Phalcon\Db::FETCH_ASSOC, $bind);
            $res['total_rows'] = count($res['rows']);
            if (isset($param['fetch']) && ($param['fetch'] == 'one')) {
                $res['rows'] = !empty($res['rows']) ? $res['rows'][0] : [];
                $res['total_rows'] = 1;
            }

            if (isset($param['limit']) && !empty($param['limit'])) {
                $res['total_rows'] = count(self::db()->fetchAll($sqlCount, \Phalcon\Db::FETCH_ASSOC, $bind));
            }

            if(isset($param['cache']) && $param['cache'] != false){
                $cache->save($key, json_encode($res));
            }
        }

        return $res;
    }

    public static function fetchAll($param=[]){
        return self::opt($param);
    }

    public static function fetchOne($param=[])
    {
        $data = self::opt($param);
        if (!empty($data)) {
            $data['rows'] = $data['rows'][0];
        }
        return $data;
    }

    public  static function insert($tbl_name, $data)
    {
        unset($data['_url']);
        try {
            $insert  = self::db()->insertAsDict($tbl_name, $data);
            if($insert){
                $return = [
                    'status' => 1,
                    'message' => 'insert berhasil',
                    'lastInsertId' => self::db()->lastInsertId()
                ];
            }else{
                $return = [
                    'status' => 0,
                    'message' => 'insert gagal',
                ];
            }
            return $return;
        } catch (\Exception $exc) {
            print_r($exc);   
        }
    }

    public  static function update($tbl_name, $data, $id)
    {
        unset($data['_url']);
        try {
            $update = self::db()->updateAsDict($tbl_name, $data, $id);
            if($update){
                $return = [
                    'status' => 1,
                    'message' => 'update berhasil',
                ];
            }else{
                $return = [
                    'status' => 0,
                    'message' => 'update gagal',
                ];
            }
            return $return;
        } catch (\Exception $exc) {
            print_r($exc);   
        }
    }
    
    public static function remove($tbl_name, $where){
//        $delete = self::db()->delete($tbl_name, $where);
//        return ($r ? 1 : 0);
        try {
            $delete = self::db()->delete($tbl_name, $where);
            if($delete){
                $return = [
                    'status' => 1,
                    'message' => 'delete berhasil',
                ];
            }else{
                $return = [
                    'status' => 0,
                    'message' => 'delete gagal',
                ];
            }
            return $return;
        } catch (\Exception $exc) {
            print_r($exc);   
        }
    }

}
